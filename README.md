# Webarchitects MailHog Ansible Role

An Ansible role to install [MailHog](https://github.com/mailhog/MailHog) and [mhsendmail](https://github.com/mailhog/mhsendmail) on Debian and Ubuntu.

See also [Guy Geerling's role](https://github.com/geerlingguy/ansible-role-mailhog), the systemd script in this repo has been copied from there.
